import * as dotenv from 'dotenv'
import * as Knex from 'knex'
import { VacancyService } from './service/VacancyService';


dotenv.config();
const knexConfig = require('./knexfile');
const knex: Knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);


const vacancyService =  new VacancyService(knex);

const create = async () => {
    await vacancyService.create();
    console.log(new Date() + ": updated")
    process.exit(0);
}

create()