 import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {
    return knex.schema.createTable("car_park_info", (table) => {
        table.increments();
        table.string("park_Id").unique().notNullable();
        table.string("name").notNullable()
        table.string("nature")
        table.string("displayAddress").notNullable()
        table.string("district")
        table.string("latitude").notNullable();
        table.string("longitude").notNullable();
        table.string("contactNo")
        table.string("website")
        table.string("opening_status");
        table.specificType("facilities", "text array");
        table.specificType("paymentMethods", "text array");
        table.string("creationDate");
        table.string("modifiedDate");
        table.string("publishedDate");
        table.enum("lang", ["en_US", "zh_TW", "zh_CN"]).notNullable();
        table.timestamps(false, true);
    })
};

exports.down = async function (knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists("car_park_info");
};
