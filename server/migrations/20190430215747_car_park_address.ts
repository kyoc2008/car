import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {

    return knex.schema.createTable("car_park_address", (table) => {
        table.increments();
        table.integer("car_park_id").unique();
        table.foreign("car_park_id", "car_park_info.id");
        table.string("unitNo");
        table.string("unitDescriptor");
        table.string("floor");
        table.string("blockNo");
        table.string("blockDescriptor");
        table.string("buildingName");
        table.string("phase");
        table.string("estateName");
        table.string("villageName");
        table.string("streetName");
        table.string("buildingNo");
        table.string("subDistrict").notNullable();
        table.string("dcDistrict").notNullable();
        table.string("region").notNullable();
        table.timestamps(false, true);

    })

};

exports.down = async function (knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists("car_park_address");
};