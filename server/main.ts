import * as express from 'express';
import * as bodyParser from 'express';
import * as expressSession from 'express-session'
import * as Knex from 'knex'
import * as dotenv from 'dotenv'
import * as cors from 'cors';
import { CarParkInfoService } from './service/CarParkInfoService';
import { VacancyService } from './service/VacancyService';
import { CarParkInfoRouter } from './router/CarParkInfoRouter';
import { VacancyRouter } from './router/VacancyRouter';

dotenv.config();
const knexConfig = require('./knexfile');
const knex: Knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);

const app = express();
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressSession({
    secret: 'Tecky Car',
    resave: true,
    saveUninitialized: true
}));


const carParkInfoService = new CarParkInfoService(knex)
const vacancyService = new VacancyService(knex)

app.use('/carparkinfo', new CarParkInfoRouter(carParkInfoService).router())
app.use('/vacancy', new VacancyRouter(vacancyService).router())

app.use(function (_req, res, _next) {
    res.end("It looks like you are accessing a wrong page!");
})

const PORT = 9090;
app.listen(PORT, function () {
    console.log(`Server listening on http://localhost:${PORT}`);
}); 