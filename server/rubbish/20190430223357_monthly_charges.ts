import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {

    return knex.schema.createTable("monthlyCharges", (table) => {
        table.increments();
        table.integer("car_id").unique();
        table.foreign("car_id", "car.id");
        table.timestamps(false, true);

        table.enum("type", [
            "monthly-park",
            "monthly-day-park",
            "monthly-night-park",
            "bimonthly-park",
            "bimonthly-day-park",
            "bimonthly-night-park",
            "quarterly-park",
            "quarterly-day-park",
            "quarterly-night-park",
            "yearly-park",
            "yearly-day-park",
            "yearly-night-park",
        ]).notNullable();
        table.integer("price").notNullable();
        table.enum("covered", ["covered", "semi-covered", "open-air", "mixed"]).notNullable()
        table.enum("reserved", ["reserved", "non-reserved"]).notNullable()
        table.string("remark")

    })

};

exports.down = async function (knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists("monthlyCharges");
};