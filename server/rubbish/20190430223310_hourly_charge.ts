import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {

    return knex.schema.createTable("hourlyCharges", (table) => {
        table.increments();
        table.integer("car_id").unique();
        table.foreign("car_id", "car.id");
        table.timestamps(false, true);

        table.enum("type", ["hourly", "half-hourly"]).notNullable();
        table.enum("weekdays", ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN", "PH"]).notNullable();
        table.boolean("excludePublicHoliday").notNullable();
        table.string("periodStart").notNullable();
        table.string("periodEnd").notNullable();
        table.integer("price").notNullable();
        table.json("usageThresholds") //important t_t
        table.enum("covered",["covered", "semi-covered", "open-air", "mixed"]).notNullable()
        table.string("remark");
    })

};

exports.down = async function (knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists("hourlyCharges");
};