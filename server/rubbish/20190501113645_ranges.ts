import * as Knex from "knex";

exports.up = async function (knex: Knex): Promise<any> {
    return knex.schema.createTable("ranges", (table) => {
        table.increments();
        table.integer("monthly_charges").unique();
        table.foreign("monthly_charges", "monthly_charges.id");
        table.timestamps(false, true);


        table.enum("weekdays", ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN", "PH"]).notNullable();
        table.boolean("excludePublicHoliday").notNullable();
        table.string("periodStart").notNullable();
        table.string("periodEnd").notNullable();
    })
};

exports.down = async function (knex: Knex): Promise<any> {
    return knex.schema.dropTableIfExists("ranges");
};
