import { IVacancy } from '../model';
import * as Knex from 'knex';
import axios from 'axios';
import { IVacancyService } from './IVacancyService';
import { Vacancy } from '../vacancy_model';


export class VacancyService implements IVacancyService {
    constructor(private knex: Knex) {

    }
    async create(): Promise<void> {
        try {
            const vacancy: Vacancy = (await axios('https://api.data.gov.hk/v1/carpark-info-vacancy?data=vacancy')).data
           
            await this.knex.transaction(async (trx) => {
                for (let carParkVacancy of vacancy.results) {
                    const check = await trx.select("id").from('car_park_vacancy').where("park_Id", carParkVacancy.park_Id).limit(1)
                    if(check.length === 0 ){
                        await trx.insert(carParkVacancy).into('car_park_vacancy');
                    }else{
                        await trx.update(carParkVacancy).into('car_park_vacancy').where("park_Id", carParkVacancy.park_Id);
                    }
                }
            })
        } catch (e) {
            throw new Error(e)
        }
    }

    async retrieve(): Promise<IVacancy[]> {

        try {
            return this.knex.select([
                "*"
            ]).from('car_park_vacancy').orderBy('id');
        } catch (e) {
            throw new Error(e)
        }
    }
}

