

export interface ICarParkInfoService {
    create: () => Promise<void>;
    retrieveInfo: () => Promise<JSON>;
    retrievePrice: () => Promise<JSON>;
    // update: () => Promise<void>;
    // delete: () => Promise<void>;
}

