import * as Knex from 'knex';
import { ICarParkInfoService } from './ICarParkInfoService'
import { Info, Result } from '../info_model';
import axios from 'axios'
import { IEnclosedElement } from '../model';

export class CarParkInfoService implements ICarParkInfoService {

    constructor(private knex: Knex) {

    }

    async create(): Promise<void> {
        const keys: (keyof Result)[] = [
            "park_Id",
            "name",
            "nature",
            "displayAddress",
            "district",
            "latitude",
            "longitude",
            "contactNo",
            "website",
            "opening_status",
            "facilities",
            "paymentMethods",
            "creationDate",
            "modifiedDate",
            "publishedDate",
            "lang",
        ]

        const carTypes: (keyof Result)[] = [
            "privateCar",
            "LGV",
            "HGV",
            "CV",
            "coach",
            "motorCycle"
        ]

        try {
            const infos: Info = (await axios('https://api.data.gov.hk/v1/carpark-info-vacancy?data=info')).data
            return this.knex.transaction(async (trx: Knex.Transaction) => {
                for (let carPark of infos.results) {

                    // console.log("basic info part");
                    // basic info part
                    const info = {}
                    keys.map((key) => {
                        if (carPark[key]) {
                            Object.assign(info, { [key]: carPark[key] })
                        }
                    })
                    const check = await trx.select("id").from('car_park_info').where("park_Id", carPark.park_Id).limit(1)
                    let carParkId: number[];
                    if (check.length === 0) {
                        carParkId = await trx.insert(info).into('car_park_info').returning("id");
                    } else {
                        carParkId = await trx.update(info).into('car_park_info').where("park_Id", carPark.park_Id).returning("id");
                    }



                    // console.log("address part");
                    //address part
                    if (carPark.address) {
                        const address = { ...carPark.address, car_park_id: carParkId[0] };
                        const checkAddress = await trx.select("id").from('car_park_address').where("car_park_id", carParkId[0]).limit(1)
                        if (checkAddress.length === 0) {
                            await trx.insert(address).into("car_park_address");
                        } else {
                            await trx.update(address).into('car_park_address').where("car_park_id", carParkId[0]);
                        }
                    }

                    // console.log("rendition part");
                    //rendition part
                    if (carPark.renditionUrls) {
                        const renditionUrls = { ...carPark.renditionUrls, car_park_id: carParkId[0] };
                        const checkHas = await trx.select("id").from('car_park_rendition').where("car_park_id", carParkId[0]).limit(1)
                        if (checkHas.length === 0) {
                            await trx.insert(renditionUrls).into("car_park_rendition");
                        } else {
                            await trx.update(renditionUrls).into('car_park_rendition').where("car_park_id", carParkId[0]);
                        }
                    }

                    // console.log("opening hour part");
                    // opening hour part
                    if (carPark.openingHours) {
                        const data = { opening_hour: carPark.openingHours, car_park_id: carParkId[0] };
                        const checkHas = await trx.select("id").from('car_park_opening').where("car_park_id", carParkId[0]).limit(1)
                        if (checkHas.length === 0) {
                            await trx.insert(data).into("car_park_opening");
                        } else {
                            await trx.update(data).into('car_park_opening').where("car_park_id", carParkId[0]);
                        }
                    }

                    // console.log("height limit part");
                    //height limit part
                    if (carPark.heightLimits) {
                        const heightLimits = { height_limit: carPark.heightLimits, car_park_id: carParkId[0] };
                        const checkHas = await trx.select("id").from('car_park_hl').where("car_park_id", carParkId[0]).limit(1)
                        if (checkHas.length === 0) {
                            await trx.insert(heightLimits).into("car_park_hl");
                        } else {
                            await trx.update(heightLimits).into('car_park_hl').where("car_park_id", carParkId[0]);
                        }
                    }

                    // console.log("cartype and charge");
                    //cartype and charge
                    // const carParkId = ['10']
                    for (let carType of carTypes) {
                        if (carPark[carType]) {
                            const park_Id = await trx.select('park_Id').from('car_park_info').where('id', carParkId[0]);
                            const enclosedElement = carPark[carType] as IEnclosedElement;
                            const data = { ...enclosedElement, type: carType, park_id: park_Id[0].park_Id };
                            const checkHas = (
                                await trx.select("id").from('car_park_cars').where( "park_id", park_Id[0].park_Id).andWhere("type", carType).limit(1)
                                );
                            if (!checkHas[0]) {
                                await trx.insert(data).into("car_park_cars").returning("id");
                            } else {
                                await trx.update(data).into('car_park_cars').where("park_id", park_Id[0].park_Id).where("type", carType);
                            }
                        }
                    }
                }
            });

        } catch (e) {
            throw new Error(e)
        }
    }

    async retrieveInfo(): Promise<JSON> {
        try {
            const basicInfo = await this.knex.select([
                "park_Id",
                "name",
                "nature",
                "displayAddress",
                "district",
                "latitude",
                "longitude",
                "contactNo",
                "website",
                "opening_status",
                "facilities",
                "paymentMethods",
                "creationDate",
                "modifiedDate",
                "publishedDate",
                "lang",
                "carpark_photo",
            ])
                .from('car_park_info')
                .fullOuterJoin("car_park_rendition", "car_park_info.id", "car_park_rendition.car_park_id")

            return basicInfo;

        } catch (e) {
            throw new Error(e)
        }
    }

    async retrievePrice(): Promise<JSON> {
        try {
            return this.knex.select([
                'park_id',
                'type',
                'hourlyCharges',
                'dayNightParks',
                'monthlyCharges'
            ]).from('car_park_cars')
            // .where('type', carType).andWhere("car_park_id", parkId).limit(1)
        } catch (e) {
            throw new Error(e)
        }
    }
}
