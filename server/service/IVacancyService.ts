import {IVacancy } from '../model';



export interface IVacancyService {
    create: () => Promise<void>;
    retrieve: () => Promise<IVacancy[]>;
    // update: () => Promise<void>;
    // delete: () => Promise<void>;
}

