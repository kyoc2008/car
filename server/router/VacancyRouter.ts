import * as express from 'express';
import { Request, Response } from 'express';
import { IVacancyService } from '../service/IVacancyService';



export class VacancyRouter {

    constructor(private vacancyService: IVacancyService) {

    }

    router() {
        const router = express.Router();
        router.get('/', this.get);
        router.post('/', this.post);
        // router.put('/', this.put);
        // router.delete('/', this.delete);
        return router;
    }

    get = async (_req: Request, res: Response) => {
        try {
            res.json(await this.vacancyService.retrieve());
        } catch (e) {
            console.log(e);
            res.status(500);
            res.json({ success: false });
        }
    }

    post = async (_req: Request, res: Response) => {
        try {
            await this.vacancyService.create();
            res.json({ success: true });
        } catch (e) {
            console.log(e);
            res.status(500);
            res.json({ success: false });
        }
    }

    // put = async (req: Request, res: Response) => {
    //     try {
    //         const updated = req.body;
    //         const id = req.query.id;
    //         await this.userService.update(id, updated);
    //         res.json({ success: true });
    //     } catch (e) {
    //         console.log(e);
    //         res.status(500);
    //         res.json({ success: false });
    //     }
    // }

    // delete = async (req: Request, res: Response) => {
    //     try {
    //         const id = req.query.id;
    //         await this.userService.delete(id);
    //         res.json({ success: true });
    //     } catch (e) {
    //         console.log(e);
    //         res.status(500);
    //         res.json({ success: false });
    //     }
    // }
}