import * as express from 'express';
import { Request, Response } from 'express';
import { ICarParkInfoService } from '../service/ICarParkInfoService';


export class CarParkInfoRouter {

    constructor(private carParkInfoService: ICarParkInfoService) {

    }

    router() {
        const router = express.Router();
        router.get('/', this.get);
        router.get('/charge', this.getCharge);
        router.post('/', this.post);
    
        return router;
    }

    get = async (_req: Request, res: Response) => {
        try {
            res.json(await this.carParkInfoService.retrieveInfo());
        } catch (e) {
            console.log(e);
            res.status(500);
            res.json({ success: false });
        }
    }
    getCharge = async (_req: Request, res: Response) => {
        try {
            res.json(await this.carParkInfoService.retrievePrice());
        } catch (e) {
            console.log(e);
            res.status(500);
            res.json({ success: false });
        }
    }

    post = async (_req: Request, res: Response) => {
        try {
            await this.carParkInfoService.create();
            res.json({ success: true });
        } catch (e) {
            console.log(e);
            res.status(500);
            res.json({ 
                success: false ,
                e: e
            });
        }
    }

    // put = async (req: Request, res: Response) => {
    //     try {
    //         const updated = req.body;
    //         const id = req.query.id;
    //         await this.userService.update(id, updated);
    //         res.json({ success: true });
    //     } catch (e) {
    //         console.log(e);
    //         res.status(500);
    //         res.json({ success: false });
    //     }
    // }

    // delete = async (req: Request, res: Response) => {
    //     try {
    //         const id = req.query.id;
    //         await this.userService.delete(id);
    //         res.json({ success: true });
    //     } catch (e) {
    //         console.log(e);
    //         res.status(500);
    //         res.json({ success: false });
    //     }
    // }
}