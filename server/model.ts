export interface IEnclosedElement{
    space: number;
    spaceDIS: number;
    spaceEV: number;
    spaceUNL: number;
    hourlyCharges: {
        type: PriceType;
        weekdays: weekdays[]
        excludePublicHoliday: boolean;
        periodStart: string;
        periodEnd: string;
        price: number;
        usageThresholds: [{
            price: number;
            hours: number;
        }]
        covered: covered;
        remark: string;
        usageMinimum: number
    }[]
    dayNightParks: [{
        type: timeType;
        weekdays: weekdays[]
        excludePublicHoliday: boolean;
        periodStart: string;
        periodEnd: string;
        validUntil: validUntil;
        validUntilEnd: string;
        price: number;
        covered: covered;
        remark: string;
    }]
    monthlyCharges: {
        type: monthtypes;
        price: number;
        ranges: {
            weekdays: weekdays[]
            excludePublicHoliday: boolean;
            periodStart: string;
            periodEnd: string;
        }[];
        covered: covered;
        reserved: reserved;
        remark: string;
    }[]
    unloadings: {
        type: PriceType;
        price: number;
        usageThresholds: [{
            price: number;
            hours: number;
        }];
        remark: string;
    }[]
    privileges: {
        weekdays: weekdays[];
        excludePublicHoliday: boolean;
        periodStart: string;
        periodEnd: string;
        description: string
    }[]
}

enum PriceType {
    "hourly",
    "half-hourly",
}

enum weekdays{
    "MON",
    "TUE",
    "WED",
    "THU",
    "FRI",
    "SAT",
    "SUN",
    "PH",
}

enum covered{
    "covered",
    "semi-covered",
    "open-air",
    "mixed",
}

enum timeType{
    "day-park",
    "night-park",
    "6-hours-park",
    "hours-park",
    "24-hours-park",
}

enum validUntil{
    "no-restrictions",
    "same-day",
    "ollowing-day"
}

enum monthtypes{
    "monthly-park",
    "monthly-day-park",
    "monthly-night-park",
    "bimonthly-park",
    "bimonthly-day-park",
    "bimonthly-night-park",
    "quarterly-park",
    "quarterly-day-park",
    "quarterly-night-park",
    "yearly-park",
    "early-day-park",
    "yearly-night-park",
}

 enum reserved{
    "reserved",
    "non-reserved"
}

export interface IVacancyInfo {
    park_Id: string;
    privateCar?: [{
        vacancy?: number
        vacancyEV?: number
        vacancyDIS?: number
    }]
    LGV?: [{
        vacancy?: number
        vacancyEV?: number
        vacancyDIS?: number
    }];
    HGV?: [{
        vacancy?: number
        vacancyEV?: number
        vacancyDIS?: number
    }];
    CV?: [{
        vacancy?: number
        vacancyEV?: number
        vacancyDIS?: number
    }];
    coach?: [{
        vacancy?: number
        vacancyEV?: number
        vacancyDIS?: number
    }];
    motorCycle?: [{
        vacancy?: number
        vacancyEV?: number
        vacancyDIS?: number
    }];
}

export interface IVacancyInput {
    results: IVacancyInfo[]
}
export interface IVacancy {
    park_Id: string,
    privateCar?: number,
    privateCarEV?: number,
    privateCarDIS?: number,
    LGV?: number,
    HGV?: number,
    CV?: number,
    coach?: number,
    motorCycle?: number
}

// export interface ICarPark {
//     results: {
//         park_Id: string; //Y
//         name: string; //Y
//         nature: string;
//         displayAddress: string;//Y
//         district: string;
//         latitude: string;
//         longtitude: string;
//         contactNo: string;
//         reditionUrls: ICarParkRendition;
//         website: string;
//         opening_status: string;
//         openingHours: ICarParkOpening[];
//         heightLimits: ICarParkHL[];
//         facilities: ICarParkFacilities[];
//         paymentMethods: string[];
//         priavateCar: IEnclosedElement;
//         LGV: IEnclosedElement;
//         HGV: IEnclosedElement;
//         CV: IEnclosedElement;
//         coach: IEnclosedElement;
//         motorCycle: IEnclosedElement;
//         creationDate: string;
//         modifiedDate: string;
//         publishedDate: string;
//         lang: string;
//     }[];
// }

// export interface ICarParkRendition{
//     square: string;
//     thumbnail: string;
//     banner: string;
// }

// export interface ICarParkOpening{
//     weekdays: weekdays[];
//     excludePublicHoliday: boolean;
//     periodStart: string;
//     periodEnd: string;
// }

