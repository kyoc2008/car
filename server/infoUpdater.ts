import * as dotenv from 'dotenv'
import * as Knex from 'knex'
import { CarParkInfoService } from './service/CarParkInfoService';


dotenv.config();
const knexConfig = require('./knexfile');
const knex: Knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);


const service =  new CarParkInfoService(knex);

const create = async () => {
    await service.create()
    console.log(new Date() + ": updated")
    process.exit(0);
}

create()