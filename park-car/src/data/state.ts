import { CarType } from "../service/state";

export interface IDataState {
    carParkInfo: ICarParkInfo[];
    carParkVacancy: ICarParkVacancy[];
    chargeInfo: IChargeInfo[]
}
export interface ICarParkVacancy {
    park_Id: string;
    privateCar?: Placeholder[];
    LGV?: Placeholder[];
    HGV?: Placeholder[];
    motorCycle?: Placeholder[];
    CV?: Placeholder[];
    coach?: Placeholder[];
}
export interface ICarParkInfo {
    park_Id: string;
    name: string;
    nature?: Nature;
    displayAddress: string;
    district?: string;
    latitude: number;
    longitude: number;
    contactNo: string;
    website: string;
    opening_status: OpeningStatus;
    facilities?: Facility[];
    paymentMethods?: PaymentMethod[];
    creationDate?: string;
    modifiedDate?: string;
    publishedDate?: string;
    lang: Lang;
    carpark_photo: string,
    height_limit: HeightLimit[],
}

export interface IChargeInfo {
    park_id: string;
    type: CarType;
    hourlyCharges?: {
        type: PriceType;
        weekdays: weekdays[]
        excludePublicHoliday: boolean;
        periodStart: string;
        periodEnd: string;
        price: number;
        usageThresholds: [{
            price: number;
            hours: number;
        }]
        covered: covered;
        remark: string;
        usageMinimum: number
    }[];
    dayNightParks?: {
        type: timeType;
        weekdays: weekdays[]
        excludePublicHoliday: boolean;
        periodStart: string;
        periodEnd: string;
        validUntil: validUntil;
        validUntilEnd: string;
        price: number;
        covered: covered;
        remark: string;
    }[];
    monthlyCharges?: {
        type: monthtypes;
        price: number;
        ranges: {
            weekdays: weekdays[]
            excludePublicHoliday: boolean;
            periodStart: string;
            periodEnd: string;
        }[];
        covered: covered;
        reserved: reserved;
        remark: string;
    }[];
}
interface HeightLimit {
    height: number;
    remark?: string;
}

export interface Placeholder {
    vacancy_type: VacancyType;
    vacancy: number;
    lastupdate: string;
    vacancyUNL?: number;
    vacancyEV?: number;
    vacancyDIS?: number;
    category?: Category;
}



enum Nature {
    Commercial = "commercial",
    Government = "government",
}

export enum OpeningStatus {
    Closed = "CLOSED",
    Open = "OPEN",
}

enum PaymentMethod {
    AutopayStation = "autopay-station",
    Cash = "cash",
    Master = "master",
    Octopus = "octopus",
    Visa = "visa",
    EPS = "EPS",
    Unionpay = "unionpay",
    Alipay = 'alipay',
}

enum Facility {
    Disabilities = "disabilities",
    EvCharger = "evCharger",
    Unloading = "unloading",
    Washing = "washing",
    ValetParking = "valet-parking"
}

export enum Lang {
    EnUS = "en_US",
    ZhTW = "zh_TW",
    ZhCN = "zh_CN"
}

enum VacancyType {
    A = "A",
    B = "B",
    C = "C"
}

enum Category {
    Hourly = "HOURLY",
    Daily = "DAILY",
    Monthly = "MONTHLY",
}

enum PriceType {
    "hourly",
    "half-hourly",
}

enum weekdays{
    "MON",
    "TUE",
    "WED",
    "THU",
    "FRI",
    "SAT",
    "SUN",
    "PH",
}

enum covered{
    "covered",
    "semi-covered",
    "open-air",
    "mixed",
}

enum timeType{
    "day-park",
    "night-park",
    "6-hours-park",
    "hours-park",
    "24-hours-park",
}

enum validUntil{
    "no-restrictions",
    "same-day",
    "ollowing-day"
}

enum monthtypes{
    "monthly-park",
    "monthly-day-park",
    "monthly-night-park",
    "bimonthly-park",
    "bimonthly-day-park",
    "bimonthly-night-park",
    "quarterly-park",
    "quarterly-day-park",
    "quarterly-night-park",
    "yearly-park",
    "early-day-park",
    "yearly-night-park",
}

 enum reserved{
    "reserved",
    "non-reserved"
}