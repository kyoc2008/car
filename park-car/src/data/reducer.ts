import { IDataState, OpeningStatus, Lang } from "./state";
import { IDataAction } from "./action";

const initialState: IDataState = {
    carParkInfo: [],
    carParkVacancy: [],
    chargeInfo: []
}
// [{
//     info:{
//         park_Id: "NA",
//         name: "NA",
//         displayAddress: "NA",
//         latitude: 0,
//         longitude: 0,
//         contactNo: "NA",
//         website: "NA",
//         opening_status: OpeningStatus.Closed,
//         lang: Lang.EnUS,
//         carpark_photo: "NA",
//         height_limit: [],
//     },
//     disPlay: false
// }]

export const dataReducer = (state: IDataState = initialState, action: IDataAction): IDataState => {
    switch (action.type) {
        case "INITCARPARKINFO":
            return {
                ...state,
                carParkInfo: action.carParkInfo.map((data) => {
                    return data
                })
            }
        case "CARPARKVACANCY":
            return {
                ...state,
                carParkVacancy: action.carParkVacancy
            };
        case "CAR_PARK_CHARGE":
            return {
                ...state,
                chargeInfo: action.carParkCharge
            }
        default:
            return state;
    }
}