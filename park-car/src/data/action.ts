import { Dispatch } from 'redux';
import { ThunkResult } from '../store';
import { ICarParkInfo, ICarParkVacancy, IChargeInfo } from './state';

export const fetchCarParkInfo = (): ThunkResult<void> => {
    return async (dispatch: Dispatch<IDataAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/carparkinfo`);
        const carParkInfo: ICarParkInfo[] = await res.json();
        dispatch(initCarParkInfoAction(carParkInfo));
    }
}

export const fetchVacancy = (): ThunkResult<void> => {
    return async (dispatch: Dispatch<IDataAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/vacancy`);
        const carParkVacancy: ICarParkVacancy[] = await res.json();
        dispatch(updateCarParkVacancyAction(carParkVacancy));
    }
}
export const fetchCarParkCharge = (): ThunkResult<void> => {
    return async (dispatch: Dispatch<IDataAction>) => {
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/carparkinfo/charge`);
        const chargeInfo: IChargeInfo[] = await res.json();
        dispatch(initCarParkChargeAction(chargeInfo));
    }
}
//thunk
const initCarParkInfoAction = (carParkInfo: ICarParkInfo[]): IUpdateCarParkInfoAction => {
    return {
        type: "INITCARPARKINFO",
        carParkInfo: carParkInfo
    }
}

//thunk
const updateCarParkVacancyAction = (carParkVacancy: ICarParkVacancy[]): IUpdateCarParkVacancyAction => {
    return {
        type: "CARPARKVACANCY",
        carParkVacancy
    }
}
const initCarParkChargeAction = (carParkCharge: IChargeInfo[]): IUpdateCarParkChargeAction => {
    return {
        type: "CAR_PARK_CHARGE",
        carParkCharge
    }
}

interface IUpdateCarParkInfoAction {
    type: INITCARPARKINFO;
    carParkInfo: ICarParkInfo[]
}
interface IUpdateCarParkVacancyAction {
    type: CARPARKVACANCY;
    carParkVacancy: ICarParkVacancy[]
}
interface IUpdateCarParkChargeAction {
    type: CAR_PARK_CHARGE;
    carParkCharge: IChargeInfo[]
}

type INITCARPARKINFO = "INITCARPARKINFO"
type CARPARKVACANCY = "CARPARKVACANCY"
type CAR_PARK_CHARGE = "CAR_PARK_CHARGE"

export type IDataAction = IUpdateCarParkInfoAction | IUpdateCarParkVacancyAction | IUpdateCarParkChargeAction