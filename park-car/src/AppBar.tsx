import React from 'react';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { createStyles, Theme, withStyles, WithStyles, MuiThemeProvider } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/MyLocation';
import { IRootState, ThunkDispatch } from './store';
import { switchDrawer, changeSearchBoxAction, movePositionAction } from './service/action';
import ReactDOM from 'react-dom';
import './AppBar.css'
import { Coords } from './service/state';
import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#537380'
    }
  }
});

const styles = (theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
    title: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block',
        width: '150px'
      },
    },
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      marginRight: theme.spacing.unit * 2,
      marginLeft: 0,
      width: '100%',
      // [theme.breakpoints.up('sm')]: {
      //   marginLeft: theme.spacing.unit * 3,
      //   width: 'auto',
      // },
    },
    searchIcon: {
      width: theme.spacing.unit * 9,
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputRoot: {
      color: 'inherit',
      width: '100%',
    },
    inputInput: {
      paddingTop: theme.spacing.unit,
      paddingRight: theme.spacing.unit,
      paddingBottom: theme.spacing.unit,
      paddingLeft: theme.spacing.unit * 10,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: 200,
      },
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex',
      },
    },
    sectionMobile: {
      display: 'flex',
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
  });

interface Props extends WithStyles<typeof styles> {
  menuSwitch: () => void;
  searchBoxChange: (value: string) => void;
  changePosition: (value: Coords) => void;
  searchBoxValue: string;
  currentPosition: Coords | null;
}


class TopAppBar extends React.Component<Props> {

  private searchBox: any;

  private handleSearchBoxChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
    this.props.searchBoxChange(event.target.value);
  }

  private onPlacesChanged = () => {
    const place = this.searchBox.getPlaces();
    const lat: number = place[0].geometry.location.lat();
    const lng: number = place[0].geometry.location.lng();
    if (lat && lng) {
      this.props.changePosition({
        lat: lat,
        lng: lng
      })
    }
    this.props.searchBoxChange("");
  }

  componentDidMount = () => {
    const Google = window.google;

    if (Google) {
      const defaultBounds = new Google.maps.LatLngBounds(
        new Google.maps.LatLng(22.730072750096227, 113.8680569850194),
        new Google.maps.LatLng(21.953988742005464, 114.38304111587877)
      );

      const input = ReactDOM.findDOMNode(this.refs.input);

      this.searchBox = new Google.maps.places.SearchBox(input, {
        bounds: defaultBounds
      });

      this.searchBox.addListener('places_changed', this.onPlacesChanged);
    }
  }

  private handleIconClick = () => {
    if (this.props.currentPosition) {
      console.log("here");
      const pos = this.props.currentPosition
      this.props.changePosition(pos);
    }
  }
  render() {

    const { classes } = this.props;

    return (
      <MuiThemeProvider theme={theme}>
        <div className={classes.root}>
          <AppBar position="static">
            <Toolbar>
              <IconButton className={classes.menuButton} color="inherit" aria-label="Open drawer" onClick={this.props.menuSwitch}>
                <MenuIcon />
              </IconButton>

              <Typography className={classes.title} variant="title" color="inherit" noWrap>
                隨你泊
                </Typography>

              <div className={classes.search}>
                <input
                  placeholder="你想泊邊..."
                  value={this.props.searchBoxValue}
                  onChange={this.handleSearchBoxChange}
                  ref={"input"}
                  id={'inputBox'}
                />
              </div>
              <div className={classes.grow} />
              <IconButton color="inherit" onClick={this.handleIconClick}>
                <SearchIcon />
              </IconButton>
            </Toolbar>
          </AppBar>
        </div>
      </MuiThemeProvider>
    )
  }
}

const mapStateToProps = (state: IRootState) => {
  return {
    searchBoxValue: state.service.searchBoxValue,
    currentPosition: state.service.currentPosition
  }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
  menuSwitch: () => dispatch(switchDrawer("MENU")),
  searchBoxChange: (value: string) => dispatch(changeSearchBoxAction(value)),
  changePosition: (value: Coords) => dispatch(movePositionAction(value))
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(TopAppBar));

