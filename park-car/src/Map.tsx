import './Map.css'
import React from 'react';
import Marker from './Marker';
import CurrentMarker from './CurrentMarker';
import GoogleMapReact, { Maps, ChangeEventValue, Coords } from 'google-map-react';
import { connect } from 'react-redux';
import { IRootState, ThunkDispatch } from './store';
import { ICarParkInfo, ICarParkVacancy, IChargeInfo } from './data/state';
import { MapChangeAction, changeCurrentPosition, saveMap, movePositionAction } from './service/action';
import { IMapState, IDrawerState, CarType } from './service/state';
import { fetchCarParkInfo, fetchVacancy, fetchCarParkCharge } from './data/action';
import { getVacancy, getPrice, getVacancyType } from './Utility';
import Chip from '@material-ui/core/Chip/Chip';
import { faTruck, faTruckPickup, faCar, faClock, faSun, faCalendarAlt, faCarSide, faDollarSign, faQuestionCircle, faMap, faMotorcycle, IconDefinition } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

interface IMapProps {
    infoId: string | null;
    carParkInfo: ICarParkInfo[];
    carParkVacancy: ICarParkVacancy[];
    carParkCharge: IChargeInfo[];
    drawerState: IDrawerState;
    mapState: IMapState;
    map: any;
    currentPosition: Coords | null;
    position: Coords | null;
    mapChange: (value: ChangeEventValue) => void;
    fetchCarParkInfo: () => void;
    changeCurrentPosition: (value: Coordinates) => void;
    movePosition: (value: Coords) => void;
    fetchCarVacancy: () => void;
    fetchCharge: () => void;
    saveMap: (map: any, directionsDisplay: any) => void;
}

class Map extends React.Component<IMapProps>{

    private createMapOptions = (maps: Maps) => {
        return {
            panControl: false,
            scrollwheel: true,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false,
            styles: [
                {
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#6195a0"
                        }
                    ]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "lightness": "0"
                        },
                        {
                            "saturation": "0"
                        },
                        {
                            "color": "#f5f5f2"
                        },
                        {
                            "gamma": "1"
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "all",
                    "stylers": [
                        {
                            "lightness": "-3"
                        },
                        {
                            "gamma": "1.00"
                        }
                    ]
                },
                {
                    "featureType": "landscape.natural.terrain",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#bae5ce"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 45
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#fac9a9"
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "color": "#4e4e4e"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#787878"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.airport",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "hue": "#0a00ff"
                        },
                        {
                            "saturation": "-77"
                        },
                        {
                            "gamma": "0.57"
                        },
                        {
                            "lightness": "0"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.rail",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#43321e"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.rail",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "hue": "#ff6c00"
                        },
                        {
                            "lightness": "4"
                        },
                        {
                            "gamma": "0.75"
                        },
                        {
                            "saturation": "-68"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#eaf6f8"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#c7eced"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "lightness": "-49"
                        },
                        {
                            "saturation": "-53"
                        },
                        {
                            "gamma": "0.79"
                        }
                    ]
                }
            ]
        }
    }

    private markersMaker = () => {

        const { carType, isEV, isDisable, paymentMethod } = this.props.drawerState
        return (
            this.props.carParkInfo ? this.props.carParkInfo.map((data, index) => this.checkDisplay(getVacancy(data.park_Id, this.props.carParkVacancy, carType, isEV, isDisable)) ? (
                <Marker
                    //must pass lng and lat at here!!!
                    lng={data.longitude}
                    lat={data.latitude}
                    id={data.park_Id}
                    key={index}
                    vacancy={getVacancy(data.park_Id, this.props.carParkVacancy, carType, isEV, isDisable)}
                    vacancyType={getVacancyType(data.park_Id, this.props.carParkVacancy, carType)}
                    price={getPrice(data.park_Id, carType, paymentMethod, this.props.carParkCharge)}
                />
            ) : null) : null
        )
    }

    private currentMarker = () => {
        const { currentPosition } = this.props
        return (
            currentPosition ?
                <CurrentMarker
                    //must pass lng and lat at here!!!
                    lng={currentPosition.lng}
                    lat={currentPosition.lat}
                />
                : null)
    }

    private checkDisplay = (vacancy: number): boolean => {
        if (vacancy >= 1) {
            return true
        } else {
            return false
        }
    }

    public async componentDidMount() {
        if (navigator.geolocation) {
            if (navigator.geolocation.getCurrentPosition && navigator.geolocation.watchPosition) {
                navigator.geolocation.watchPosition((pos: Position) => {
                    this.props.changeCurrentPosition(pos.coords);
                })
                navigator.geolocation.getCurrentPosition((pos: Position) => {
                    this.props.movePosition({
                        lat: pos.coords.latitude,
                        lng: pos.coords.longitude
                    });
                })
            }
        }
        this.props.fetchCarParkInfo();
        this.props.fetchCarVacancy();
        this.props.fetchCharge();
        setInterval(() => {
            this.props.fetchCarVacancy();
        }, 60000);
    }

    private handleApiLoaded = (map: any, maps: any) => {
        const google = window.google;
        const directionsDisplay = new google.maps.DirectionsRenderer();
        this.props.saveMap(map, directionsDisplay);
    }

    render() {

        const lazy: {
            [T in CarType]: {
                icon: IconDefinition;
                displayText: string;
            }
        } = {
            [CarType.privateCar] : {icon: faCar, displayText: '私家車'},
            [CarType.lightGoodsVehicle]: {icon: faTruckPickup, displayText: '輕型貨車'},
            [CarType.heavyGoodsVehicle]: {icon: faTruck, displayText: '重型貨車'},
            [CarType.motorCycle]: {icon: faMotorcycle, displayText: '電單車'},
        }
        return (
            // Important! Always set the container height explicitly
            <div id="map">
                <Chip
                    className='chip'
                    icon={<FontAwesomeIcon icon={lazy[this.props.drawerState.carType].icon} />}
                    label={lazy[this.props.drawerState.carType].displayText}
                    color="secondary"
                    variant="default"
                />


                <GoogleMapReact
                    bootstrapURLKeys={{
                        key: "AIzaSyDoGcWqt--7YMyBfaSOVB1dG1r5RHtJeHA",
                    }}
                    options={this.createMapOptions}
                    onChange={(value) => { this.props.mapChange(value) }}
                    center={this.props.position ? this.props.position : undefined}
                    zoom={this.props.currentPosition ? 14 : 12}
                    defaultZoom={12}
                    defaultCenter={{
                        lat: 22.302558258642122,
                        lng: 114.1893797576713
                    }}
                    yesIWantToUseGoogleMapApiInternals
                    onGoogleApiLoaded={({ map, maps }) => this.handleApiLoaded(map, maps)}
                >
                    {this.markersMaker()}
                    {this.currentMarker()}
                </GoogleMapReact>

            </div >
        );
    }
}
const mapStateToProps = (state: IRootState) => {
    return {
        carParkInfo: state.data.carParkInfo,
        carParkVacancy: state.data.carParkVacancy,
        carParkCharge: state.data.chargeInfo,
        mapState: state.service.mapState,
        currentPosition: state.service.currentPosition,
        position: state.service.position,
        drawerState: state.service.drawerState,
        infoId: state.service.drawerState.infoId,
        map: state.service.map
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    mapChange: (value: ChangeEventValue) => dispatch(MapChangeAction(value)),
    changeCurrentPosition: (value: Coordinates) => dispatch(changeCurrentPosition(value)),
    movePosition: (value: Coords) => dispatch(movePositionAction(value)),
    fetchCarParkInfo: () => dispatch(fetchCarParkInfo()),
    fetchCarVacancy: () => dispatch(fetchVacancy()),
    fetchCharge: () => dispatch(fetchCarParkCharge()),
    saveMap: (map: any, directionsDisplay: any) => dispatch(saveMap(map, directionsDisplay))
})

export default connect(mapStateToProps, mapDispatchToProps)(Map);

