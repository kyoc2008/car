import React, { Component } from 'react';
import { Provider } from 'react-redux';
import Map from './Map';
import store from './store';
import './App.css';
import UI from './UI';




class App extends Component {
  componentDidMount = () => {
    document.addEventListener('touchmove',
      function (e) {
        e.preventDefault();
      }, { passive: false });
  }
  render() {
    return (
      <Provider store={store}>
        <UI />
        <Map />
      </Provider>
    );
  }
}

export default App;
