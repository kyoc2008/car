import React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import LoginIcon from '@material-ui/icons/VpnKey';
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';
import { switchDrawer, changeCarType, changeDistrict, changePaymentMethod } from './service/action';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import { Divider, FormControlLabel, Switch } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTruck, faTruckPickup, faCar, faClock, faSun, faCalendarAlt, faCarSide, faDollarSign, faQuestionCircle, faMap, faMotorcycle} from '@fortawesome/free-solid-svg-icons'
import { CarType, District, PaymentMethod } from './service/state';
import { Coords } from 'google-map-react';
import './MenuDrawer.css'




const styles = {
    list: {
        width: 200,
    },
    nested: {
        paddingLeft: 40,
    },
    shorterNested: {
        paddingLeft: 20,
    },
    shortestNested: {
        paddingLeft: 5,
    },
    customfaCar: {
        width: 20
    }
};

interface Props extends WithStyles<typeof styles> {
    menuOpen: boolean,
    menuSwitch: () => void,
    carTypeOpen: boolean,
    carTypeSwitch: () => void,
    paymentMethodOpen: boolean,
    paymentMethodSwitch: () => void,
    specialRequirementOpen: boolean,
    specialRequirementSwitch: () => void,
    districtOpen: boolean,
    districtSwitch: () => void,
    hkOpen: boolean,
    hkSwitch: () => void,
    klOpen: boolean,
    klSwitch: () => void,
    ntOpen: boolean,
    ntSwitch: () => void,
    spOpen: boolean,
    spSwitch: () => void,
    isEV: boolean,
    handleEVOnChange: () => void,
    isDisable: boolean,
    handleDisableOnChange: () => void,
    handleCarTypeOnclick: (value:CarType) => void,
    handlePaymentMethodOnclick: (value:PaymentMethod) => void,
    handleDistrictOnclick: (value:District, coords:Coords, zoom: number) => void,
}

class MenuDrawer extends React.Component<Props> {

    private sideList = () => (
        <div className={this.props.classes.list}>
            <List className="drawer">
                {/* <ListItem button>
                    <LoginIcon />
                    <ListItemText primary= "登入"/>
                </ListItem> */}
                <Divider/>
                <ListItem button onClick={this.props.carTypeSwitch}>
                    <FontAwesomeIcon icon={faCarSide} />
                    <ListItemText primary= "車種"/>
                    {this.props.carTypeOpen ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={this.props.carTypeOpen} timeout="auto" unmountOnExit>
                    <List disablePadding>
                        {[  {icon: faCar, primary: "私家車", onClick: () => this.props.handleCarTypeOnclick(CarType.privateCar)},
                            {icon: faTruckPickup, primary: "輕型貨車", onClick: () => this.props.handleCarTypeOnclick(CarType.lightGoodsVehicle)},
                            {icon: faTruck, primary: "重型貨車", onClick: () => this.props.handleCarTypeOnclick(CarType.heavyGoodsVehicle)},
                            {icon: faMotorcycle, primary: "電單車", onClick: () => this.props.handleCarTypeOnclick(CarType.motorCycle)},
                        ].map((value, index)=>(
                            <ListItem button className={this.props.classes.nested} key={index} onClick={value.onClick}>
                                <div className={this.props.classes.customfaCar} ><FontAwesomeIcon icon={value.icon} /></div>
                                <ListItemText inset primary={value.primary} /> 
                            </ListItem>
                            ))
                        }
                    </List>
                </Collapse>
                <Divider/>
                <ListItem button onClick={this.props.paymentMethodSwitch}>
                        <div className={this.props.classes.customfaCar} ><FontAwesomeIcon icon={faDollarSign} /></div>
                        <ListItemText primary= "付費模式"/>
                        {this.props.paymentMethodOpen ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={this.props.paymentMethodOpen} timeout="auto" unmountOnExit>
                    <List disablePadding>
                        {[  
                            {icon: faClock, primary: "時租", onClick: () => this.props.handlePaymentMethodOnclick(PaymentMethod.hourlyCharges)},
                            {icon: faSun, primary: "日/夜租", onClick: () => this.props.handlePaymentMethodOnclick(PaymentMethod.dayNightParks)},
                            {icon: faCalendarAlt, primary: "月租", onClick: () => this.props.handlePaymentMethodOnclick(PaymentMethod.monthlyCharges)},
                            ].map((value, index) => (
                            <ListItem button className={this.props.classes.nested} onClick={value.onClick} key={index}>
                                <FontAwesomeIcon icon={value.icon} />
                                <ListItemText inset primary={value.primary} />
                            </ListItem>)
                            )}
                    </List>
                </Collapse>
                <Divider/>
                <ListItem button onClick={this.props.spSwitch}>
                    <FontAwesomeIcon icon={faQuestionCircle} />
                    <ListItemText primary= "特別要求"/>
                    {this.props.spOpen ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={this.props.spOpen} timeout="auto" unmountOnExit>
                    <List disablePadding>
                        <ListItem className={this.props.classes.nested}>
                                <FormControlLabel
                                    control={ <Switch checked={this.props.isEV}/>}
                                    label="差電"
                                    onChange={this.props.handleEVOnChange}
                                />
                        </ListItem>
                        <ListItem className={this.props.classes.nested}>
                                <FormControlLabel
                                    control={ <Switch checked={this.props.isDisable}/>}
                                    label="傷殘設施"
                                    onChange={this.props.handleDisableOnChange}
                                />
                        </ListItem>
                    </List>
                </Collapse>
            </List>
        </div>
        
    );

    render() {


        return (
            <div>
                <SwipeableDrawer
                    anchor="left"
                    open={this.props.menuOpen}
                    onClose={this.props.menuSwitch}
                    onOpen={this.props.menuSwitch}
                >
                    {this.sideList()}
                </SwipeableDrawer>
            </div >
        )
    }
}

const mapStateToProps = (state: IRootState) => {
    return {
        menuOpen: state.service.drawerState.menuOpen,
        carTypeOpen: state.service.drawerState.carTypeOpen,
        paymentMethodOpen: state.service.drawerState.paymentMethodOpen,
        specialRequirementOpen: state.service.drawerState.specialRequirementOpen,
        districtOpen: state.service.drawerState.districtOpen,
        hkOpen: state.service.drawerState.hkOpen,
        klOpen: state.service.drawerState.klOpen,
        ntOpen: state.service.drawerState.ntOpen,
        spOpen: state.service.drawerState.spOpen,
        isEV: state.service.drawerState.isEV,
        isDisable: state.service.drawerState.isDisable,

    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    menuSwitch: () => dispatch(switchDrawer("MENU")),
    carTypeSwitch: () => dispatch(switchDrawer("CARTYPE")),
    paymentMethodSwitch: () => dispatch(switchDrawer("PAYMENT")),
    specialRequirementSwitch: () => dispatch(switchDrawer("SPECIAL")),
    districtSwitch: () => dispatch(switchDrawer("DISTRICT")),
    hkSwitch: () => dispatch(switchDrawer("HK")),
    klSwitch: () => dispatch(switchDrawer("KL")),
    ntSwitch: () => dispatch(switchDrawer("NT")),
    spSwitch: () => dispatch(switchDrawer("SP")),
    handleCarTypeOnclick: (value: CarType) => dispatch(changeCarType(value)),
    handleDistrictOnclick: (value: District, coords: Coords, zoom: number) => dispatch(changeDistrict(value,coords,zoom)),
    handlePaymentMethodOnclick: (value: PaymentMethod) => dispatch(changePaymentMethod(value)),
    handleEVOnChange: () => dispatch(switchDrawer("EV")),
    handleDisableOnChange: () => dispatch(switchDrawer("DISABLE"))
})


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(MenuDrawer));





{/* <ListItem button onClick={this.props.districtSwitch}>
                        <FontAwesomeIcon icon={faMap} />
                        <ListItemText primary= "地區"/>
                        {this.props.districtOpen ? <ExpandLess /> : <ExpandMore />}
                </ListItem> */}
                {/* <Collapse in={this.props.districtOpen} timeout="auto" unmountOnExit>
                    <List disablePadding>
                        <ListItem button className={this.props.classes.shortestNested}
                            onClick={this.props.klSwitch}>
                            <ListItemText inset primary="九龍" />
                            {this.props.klOpen ? <ExpandLess /> : <ExpandMore />}
                        </ListItem>
                        <Collapse in={this.props.klOpen} timeout="auto" unmountOnExit>
                        <List disablePadding>
                        {[
                            {primary: "深水埗區", onClick: () => this.props.handleDistrictOnclick(District.shamShuiPo,{lat:22.3274076,lng:114.1513127},16)},
                            {primary: "九龍城區", onClick: () => this.props.handleDistrictOnclick(District.kowloonCity,{lat:22.330981,lng:114.1892747},17)},
                            {primary: "觀塘區", onClick: () => this.props.handleDistrictOnclick(District.kwunTong,{lat:22.3120123,lng:114.2193389},16)},
                            {primary: "黃大仙區", onClick: () => this.props.handleDistrictOnclick(District.wongTaiSin,{lat:22.3420553,lng:114.1898863},16)},
                            {primary: "油尖旺區", onClick: () => this.props.handleDistrictOnclick(District.yauTsimMong,{lat:22.3099703,lng:114.1500914},14)},
                        ].map((value, index)=>(
                                <ListItem button className={this.props.classes.shorterNested} key={index} onClick={value.onClick}>
                                    <ListItemText inset primary={value.primary} />
                                </ListItem>)
                            )}
                        </List>
                        </Collapse>
                        <ListItem button className={this.props.classes.shortestNested}
                            onClick={this.props.ntSwitch}>
                            <ListItemText inset primary="新界" />
                            {this.props.ntOpen ? <ExpandLess /> : <ExpandMore />}
                        </ListItem>
                        <Collapse in={this.props.ntOpen} timeout="auto" unmountOnExit>
                        <List disablePadding>
                        {[
                            {primary: "葵青區", onClick: () => this.props.handleDistrictOnclick(District.kwaiTsing,{lat:22.3534523,lng:114.0958334},14)},
                            {primary: "荃灣區", onClick: () => this.props.handleDistrictOnclick(District.tuenWan,{lat:22.3713646,lng:114.1084949},15.35)},
                            {primary: "屯門區", onClick: () => this.props.handleDistrictOnclick(District.tuenMun,{lat:22.3925225,lng:113.9610011},14.82)},
                            {primary: "元朗區", onClick: () => this.props.handleDistrictOnclick(District.yuenLong,{lat:22.4554553,lng:114.0251034},14.13)},
                            {primary: "北區", onClick: () => this.props.handleDistrictOnclick(District.north,{lat:22.4985756,lng:114.1283488},15.01)},
                            {primary: "大埔區", onClick: () => this.props.handleDistrictOnclick(District.taiPo,{lat:22.4479621,lng:114.164366},14.64)},
                            {primary: "沙田區", onClick: () => this.props.handleDistrictOnclick(District.shaTin,{lat:22.3827816,lng:114.1775374},14.64)},
                            {primary: "西貢區", onClick: () => this.props.handleDistrictOnclick(District.saiKung,{lat:22.3315858,lng:114.2518185},13.25)},
                            {primary: "離島區", onClick: () => this.props.handleDistrictOnclick(District.islands,{lat:22.2682178,lng:113.9584846},12.76)},
                        ].map((value, index)=>(
                                <ListItem button className={this.props.classes.shorterNested} key={index} onClick={value.onClick}>
                                    <ListItemText inset primary={value.primary} />
                                </ListItem>)
                            )}
                        </List>
                        </Collapse>
                        <ListItem button className={this.props.classes.shortestNested}
                            onClick={this.props.hkSwitch}>
                            <ListItemText inset primary="香港" />
                            {this.props.hkOpen ? <ExpandLess /> : <ExpandMore />}
                        </ListItem>
                        <Collapse in={this.props.hkOpen} timeout="auto" unmountOnExit>
                        <List disablePadding>
                        {[
                            {primary: "中西區", onClick: () => this.props.handleDistrictOnclick(District.centralWestern,{lat:22.2796337,lng:114.1457189},15.44)},
                            {primary: "灣仔區", onClick: () => this.props.handleDistrictOnclick(District.wanChai,{lat:222.2734073,lng:114.1778628},15.37)},
                            {primary: "東區", onClick: () => this.props.handleDistrictOnclick(District.eastern,{lat:22.2756648,lng:114.2059834},14)},
                            {primary: "南區", onClick: () => this.props.handleDistrictOnclick(District.southern,{lat:22.2395616,lng:114.15374},13.62)},
                        ].map((value, index)=>(
                            <ListItem button className={this.props.classes.shorterNested} key={index} onClick={value.onClick}>
                                <ListItemText inset primary={value.primary} />
                            </ListItem>)
                        )}
                        </List>
                        </Collapse>
                    </List>
                </Collapse> */}