import React from 'react';
import { connect } from 'react-redux';
import { IRootState } from './store';
import { faCar} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './CurrentMarker.css'

interface ICurrentMarkerProps {
    //at and lng must be passed by parent!!!
    lat: number;
    lng: number;
    zoom: number
}

class CurrentMarker extends React.Component<ICurrentMarkerProps> {

    private changeStyle = (zoom: number) => {
        if (zoom <= 12) {
            return {
                fontSize: 30
            }
        } else if (zoom <= 18) {
            return {
                fontSize: 30 + (zoom - 12) * 35 / 6
            }
        } else {
            return {
                fontSize: 50
            }
        }
    }

    render() {

        return (
            
            <FontAwesomeIcon icon={faCar} className="car"
            style={this.changeStyle(this.props.zoom)}/> 
        )
    }
}

const mapStateToProps = (state: IRootState) => {
    return {
        zoom: state.service.mapState.zoom
    }
}


export default connect(mapStateToProps)(CurrentMarker);
