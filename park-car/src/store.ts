import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import { logger } from 'redux-logger';
import thunk, { ThunkDispatch, ThunkAction } from 'redux-thunk';
import { IDataState } from './data/state';
import { dataReducer } from './data/reducer';
import { IDataAction } from './data/action';
import { IServiceAction } from './service/action';
import { IServiceState } from './service/state';
import { serviceReducer } from './service/reducer';

declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
        google:any
    }
}



const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

type IRootAction = IDataAction | IServiceAction

export interface IRootState {
    data: IDataState,
    service: IServiceState
}

export type ThunkResult<R> = ThunkAction<R, IRootState, null, IRootAction>

export type ThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>


export const rootReducer = combineReducers<IRootState>({
    data: dataReducer,
    service: serviceReducer
    
})

export default createStore<IRootState, IRootAction, {}, {}>(
    rootReducer,
    composeEnhancers(
        applyMiddleware(logger),
        applyMiddleware(thunk)
    ));