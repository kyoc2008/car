import React from 'react';
import { withStyles, WithStyles, Theme} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import photo from './parkphoto.jpg';
import { Divider, Badge, Link, Fab } from '@material-ui/core';
import { ICarParkInfo, ICarParkVacancy } from './data/state';
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';
import { Coords, CarType } from './service/state';
import { getVacancy, getVacancyType, } from './Utility';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTruck, faCar, faMotorcycle, faLocationArrow, faRoute, faTruckPickup, faSquare } from '@fortawesome/free-solid-svg-icons'
import './InfoCard.css'
import { switchDrawer } from './service/action';


const styles = (theme: Theme) => ({
  margin: {
    margin: theme.spacing.unit * 2,
  },
  card: {
    maxWidth: "100%",
  },
  media: {
    height: 100
  },
  info: {
    display: 'flex',
  }
});

interface IInfoCardProps extends WithStyles<typeof styles> {
  infoId: string | null,
  carParkInfos: ICarParkInfo[],
  currentPosition: Coords | null,
  carParkVacancy: ICarParkVacancy[],
  map: any,
  directionsDisplay: any,
  isEV: boolean,
  isDisable: boolean,
  infoSwitch: () => void
}

class InfoCard extends React.Component<IInfoCardProps> {

  private handleOnClick = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    const google = window.google;
    const directionsService = new google.maps.DirectionsService();
    const { infoId, carParkInfos, currentPosition, map } = this.props
    const carParkInfo = carParkInfos.find((carParkInfo) => carParkInfo.park_Id === infoId)

    if (carParkInfo && carParkInfo.latitude && carParkInfo.longitude && currentPosition) {
      const request = {
        origin: { lat: currentPosition.lat, lng: currentPosition.lng },
        destination: { lat: parseFloat(carParkInfo.latitude.toString()), lng: parseFloat(carParkInfo.longitude.toString()) },
        travelMode: 'DRIVING',
        optimizeWaypoints: true,
      };

      directionsService.route(request, (result: any, status: any) => {
        if (status == 'OK') {
          this.props.directionsDisplay.setDirections(result);
        } else {
          console.log(status);
        }
      });
      this.props.directionsDisplay.setMap(null)
      this.props.directionsDisplay.setMap(map)
    }
    this.props.infoSwitch()
  }

  private calculateDisplay = (carType: CarType): string | number | null=> {
    const { infoId, carParkInfos, classes } = this.props
    const carParkInfo = carParkInfos.find((carParkInfo) => carParkInfo.park_Id === infoId)


    if (carParkInfo && infoId) {
      const vacancy = getVacancy(infoId, this.props.carParkVacancy, carType, this.props.isEV, this.props.isDisable);
      const type = getVacancyType(infoId, this.props.carParkVacancy, carType);

      if (type === 'A' && vacancy > 0) {
        return vacancy;
      } else if (type === 'B' && vacancy > 0){
        return 'avaliable';
      }else{
        return 'full';
      }
    }
    return null;
  }

  private getCarParkInfo = () => {
    const { infoId, carParkInfos, classes } = this.props
    const carParkInfo = carParkInfos.find((carParkInfo) => carParkInfo.park_Id === infoId)

    if (carParkInfo && infoId) {
      return (
        <Card square={true}>
          <CardMedia
            className={classes.media}
            image={photo}
            title="Car Park Picture"
          />
          <CardContent>
            <Typography variant="h5" className="infoCard">{carParkInfo.name}</Typography>
            {carParkInfo.nature ? <Typography gutterBottom variant="body1" className="customFontStyle">性質: {carParkInfo.nature}</Typography> : null}
            {carParkInfo.displayAddress ? <Typography gutterBottom variant="body1" className="customFontStyle">地址: {carParkInfo.displayAddress}</Typography> : null}
            {carParkInfo.contactNo ? <Typography gutterBottom variant="body1" className="customFontStyle">聯絡電話: {carParkInfo.contactNo ? carParkInfo.contactNo : "NA"}</Typography> : null}
            {carParkInfo.website ? <Typography gutterBottom variant="body1" className="customFontStyle">網址: {carParkInfo.website ? <Link gutterBottom variant="body2" href={carParkInfo.website}>{carParkInfo.website}</Link> : "NA"}</Typography> : null}
            {carParkInfo.opening_status ? <Typography gutterBottom variant="body1" className="customFontStyle">營業狀況: {carParkInfo.opening_status ? carParkInfo.opening_status : "NA"}</Typography> : null}
            {carParkInfo.height_limit ? <Typography gutterBottom variant="body1" className="customFontStyle">高度限制: {carParkInfo.height_limit[0].height}</Typography> : null}
            {carParkInfo.facilities ? <Typography gutterBottom variant="body1" className="customFontStyle">設施: {carParkInfo.facilities.map((value) => value + "")}</Typography> : null}
            {carParkInfo.paymentMethods ? <Typography gutterBottom variant="body1" className="customFontStyle">付款方法: {carParkInfo.paymentMethods.map((value) => value + " ")}</Typography> : null}
            <Divider />
            <div className={classes.info}>
              <Badge className={classes.margin} badgeContent={this.calculateDisplay(CarType.privateCar)} color="primary">
                <FontAwesomeIcon icon={faCar} size="2x" />
              </Badge>
              <Badge className={classes.margin} badgeContent={this.calculateDisplay(CarType.lightGoodsVehicle)} color="primary">
                <FontAwesomeIcon icon={faTruckPickup} size="2x" />
              </Badge>
              <Badge className={classes.margin} badgeContent={this.calculateDisplay(CarType.heavyGoodsVehicle)} color="primary">
                <FontAwesomeIcon icon={faTruck} size="2x" />
              </Badge>
              <Badge className={classes.margin} badgeContent={this.calculateDisplay(CarType.motorCycle)} color="primary">
                <FontAwesomeIcon icon={faMotorcycle} size="2x" />
              </Badge>
            </div>
            <div className="outerRoute">
              <Fab variant="extended" onClick={this.handleOnClick}>
                <FontAwesomeIcon icon={faLocationArrow} size="2x" color="red" />
                GO
                </Fab>
            </div>

          </CardContent>
        </Card>
      )
    }
  }

  render() {
    return (
      <div>{this.getCarParkInfo()}</div>
    )
  }
}

const mapStateToProps = (state: IRootState) => {
  return {
    infoId: state.service.drawerState.infoId,
    carParkInfos: state.data.carParkInfo,
    currentPosition: state.service.currentPosition,
    carParkVacancy: state.data.carParkVacancy,
    map: state.service.map,
    directionsDisplay: state.service.directionsDisplay,
    isEV: state.service.drawerState.isEV,
    isDisable: state.service.drawerState.isDisable
  }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
  infoSwitch: () => dispatch(switchDrawer("INFO")),
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(InfoCard))
