import React from 'react';
import ParkIcon from '@material-ui/icons/LocalParking';
import Badge from '@material-ui/core/Badge';
import { connect } from 'react-redux';
import { ThunkDispatch, IRootState } from './store';
import { changeInfoDrawerIdAction, switchDrawer } from './service/action';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core';
import './Marker.css'
const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#F5DBA9'
        },
        secondary: {
            main: '#76B577'
        }
    }
});

interface IMarkerProps {
    //at and lng must be passed by parent!!!
    lat: number;
    lng: number;
    id: string;
    vacancy: number;
    vacancyType: string;
    zoom: number;
    price: number | null;
    changeInfoDrawerId: (id: string) => void;
    switchDrawer: () => void;
}

class Marker extends React.Component<IMarkerProps>{

    private handleMarkerClick = () => {
        this.props.changeInfoDrawerId(this.props.id);
        this.props.switchDrawer()
    }

    private changeStyle = (zoom: number) => {
        if (zoom <= 12) {
            return {
                fontSize: 15
            }
        } else if (zoom <= 18) {
            return {
                fontSize: 15 + (zoom - 12) * 35 / 6
            }
        } else {
            return {
                fontSize: 50
            }
        }
    }

    render() {

        const { vacancyType, vacancy } = this.props;
        return (
            <MuiThemeProvider theme={theme}>
                <div onClick={this.handleMarkerClick} className='marker'>
                    <Badge
                        max={1000}
                        badgeContent={this.props.price ? "$" + this.props.price : null}
                        color="primary">
                        <ParkIcon style={this.changeStyle(this.props.zoom)} color={vacancy > 20?"error":"default"} />
                    </Badge>
                </div>
            </MuiThemeProvider>
        )
    }
}

const mapStateToProps = (state: IRootState) => {
    return {
        zoom: state.service.mapState.zoom
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    changeInfoDrawerId: (id: string) => dispatch(changeInfoDrawerIdAction(id)),
    switchDrawer: () => dispatch(switchDrawer("INFO")),
})


export default connect(mapStateToProps, mapDispatchToProps)(Marker);
