import { ICarParkVacancy, ICarParkInfo, IChargeInfo } from "./data/state";
import { CarType, PaymentMethod } from "./service/state";



export const getVacancy = (targetId: string, carParkVacancy: ICarParkVacancy[], carType: CarType, isEV: boolean, isDisable: boolean): number => {

    const vacancy = carParkVacancy.find((value) => value.park_Id === targetId);
    if (vacancy) {
        switch (carType) {
            case "privateCar":
                if (isEV) {
                    return vacancy.privateCar ? vacancy.privateCar[0].vacancyEV ? vacancy.privateCar[0].vacancyEV : 0 : 0
                } else if (isDisable) {
                    return vacancy.privateCar ? vacancy.privateCar[0].vacancyDIS ? vacancy.privateCar[0].vacancyDIS : 0 : 0
                } else {
                    return vacancy.privateCar ? vacancy.privateCar[0].vacancy : 0
                }
            case "LGV":
                if (isEV) {
                    return vacancy.LGV ? vacancy.LGV[0].vacancyEV ? vacancy.LGV[0].vacancyEV : 0 : 0
                } else if (isDisable) {
                    return vacancy.LGV ? vacancy.LGV[0].vacancyDIS ? vacancy.LGV[0].vacancyDIS : 0 : 0
                } else {
                    return vacancy.LGV ? vacancy.LGV[0].vacancy : 0
                }
            case "HGV":
                if (isEV) {
                    return vacancy.HGV ? vacancy.HGV[0].vacancyEV ? vacancy.HGV[0].vacancyEV : 0 : 0
                } else if (isDisable) {
                    return vacancy.HGV ? vacancy.HGV[0].vacancyDIS ? vacancy.HGV[0].vacancyDIS : 0 : 0
                } else {
                    return vacancy.HGV ? vacancy.HGV[0].vacancy : 0
                }
            case "motorCycle":
                if (isEV) {
                    return vacancy.motorCycle ? vacancy.motorCycle[0].vacancyEV ? vacancy.motorCycle[0].vacancyEV : 0 : 0
                } else if (isDisable) {
                    return vacancy.motorCycle ? vacancy.motorCycle[0].vacancyDIS ? vacancy.motorCycle[0].vacancyDIS : 0 : 0
                } else {
                    return vacancy.motorCycle ? vacancy.motorCycle[0].vacancy : 0
                }
            default:
                return 0;
        }
    }
    return 0;
}
export const getVacancyType = (targetId: string, carParkVacancy: ICarParkVacancy[], carType: CarType): string => {

    const vacancy = carParkVacancy.find((value) => value.park_Id === targetId);
    if (vacancy) {
        switch (carType) {
            case "privateCar":
                return vacancy.privateCar ? vacancy.privateCar[0].vacancy_type : 'B'
            case "LGV":
                return vacancy.LGV ? vacancy.LGV[0].vacancy_type : 'B'
            case "HGV":
                return vacancy.HGV ? vacancy.HGV[0].vacancy_type : 'B'
            case "motorCycle":
                return vacancy.motorCycle ? vacancy.motorCycle[0].vacancy_type : 'B'
            default:
                return 'B';
        }
    }
    return 'B';
}

export const getPrice = (targetId: string, carType: CarType, paymentMethod: PaymentMethod, chargeInfos: IChargeInfo[]): number | null => {
    // console.log(targetId + carType + paymentMethod);

    const weekdayNumber = new Date().getDay();
    const weekday = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"][weekdayNumber]
    // const time = new Date().getHours();

    const targetInfo = chargeInfos.find((value) => value.park_id === targetId && value.type === carType)
    if (targetInfo) {
        if (paymentMethod === "hourlyCharges") {
            return targetInfo["hourlyCharges"] ? targetInfo["hourlyCharges"][0].price : null
        } else if (paymentMethod === "dayNightParks") {
            return targetInfo["dayNightParks"] ? targetInfo["dayNightParks"][0].price : null
        } else if (paymentMethod === "monthlyCharges") {
            return targetInfo["monthlyCharges"] ? targetInfo["monthlyCharges"][0].price : null
        }
    }
    return null;
}
