import { ChangeEventValue } from "google-map-react";
import { CarType, PaymentMethod, District, Coords } from "./state";


type SWITCH = 'SWITCH';
type CONTROL = 'MENU' | 'INFO' | 'CARTYPE' | 'PAYMENT' | 'SPECIAL' | 'DISTRICT' | 'HK' | 'KL' | 'NT' | 'EV' | 'SP' | 'DISABLE'
type MAPCHANGE = 'MAPCHANGE';
type CHANGE_CAR_TYPE = 'CHANGE_CAR_TYPE'
type CHANGE_PAYMENT_METHOD = 'CHANGE_PAYMENT_METHOD'
type CHANGE_DISTRICT = 'CHANGE_DISTRICT'
type CHANGE_CURRENT_POSITION = 'CHANGE_CURRENT_POSITION'
type CHANGE_SEARCH_BOX = 'CHANGE_SEARCH_BOX'
type CHANGE_INFO_DRAWER_ID = "CHANGE_INFO_DRAWER_ID"
type MOVE_POSITION = "MOVE_POSITION"
type SAVE_MAP ="SAVE_MAP"

interface ISwitchAction {
    type: SWITCH,
    CONTROL: CONTROL
}

interface IMapChangeAction {
    type: MAPCHANGE;
    value: ChangeEventValue;
}

interface IChangeCarTypeAction {
    type: CHANGE_CAR_TYPE,
    value: CarType
}

interface IChangePaymentMethodAction {
    type: CHANGE_PAYMENT_METHOD,
    value: PaymentMethod
}

interface IChangeDistrictAction {
    type: CHANGE_DISTRICT,
    value: District,
    coords: Coords,
    zoom: number
}

interface IChangeCurrentPosition {
    type: CHANGE_CURRENT_POSITION,
    value: Coordinates
}

interface IChangeSearchBoxAction {
    type: CHANGE_SEARCH_BOX,
    value: string
}

interface IChangeInfoDrawerIdAction {
    type: CHANGE_INFO_DRAWER_ID;
    id: string;
}
interface IMovePositionAction {
    type: MOVE_POSITION,
    value: Coords
}

interface ISaveMapAction {
    type: SAVE_MAP,
    map: any,
    directionsDisplay: any
}

export function movePositionAction(value: Coords): IMovePositionAction {
    return {
        type: "MOVE_POSITION",
        value: value
    }
}

export function changeCurrentPosition(value: Coordinates): IChangeCurrentPosition {
    return {
        type: "CHANGE_CURRENT_POSITION",
        value
    }
}

export function changeCarType(value: CarType): IChangeCarTypeAction {
    return {
        type: "CHANGE_CAR_TYPE",
        value
    }
}

export function changePaymentMethod(value: PaymentMethod): IChangePaymentMethodAction {
    return {
        type: "CHANGE_PAYMENT_METHOD",
        value
    }
}

export function changeDistrict(value: District, coords: Coords, zoom: number): IChangeDistrictAction {
    return {
        type: "CHANGE_DISTRICT",
        value,
        coords,
        zoom
    }
}

export function switchDrawer(control: CONTROL): ISwitchAction {
    return {
        type: "SWITCH",
        CONTROL: control
    }
}

export const MapChangeAction = (value: ChangeEventValue): IMapChangeAction => {
    return {
        type: "MAPCHANGE",
        value
    }
}

export const changeSearchBoxAction = (value: string): IChangeSearchBoxAction => {
    return {
        type: "CHANGE_SEARCH_BOX",
        value: value
    }
}

export const changeInfoDrawerIdAction = (id: string): IChangeInfoDrawerIdAction => {
    return {
        type: "CHANGE_INFO_DRAWER_ID",
        id: id
    }
}

export const saveMap = (map : any, directionsDisplay: any): ISaveMapAction => {
    return {
        type: 'SAVE_MAP',
        map,
        directionsDisplay
    }
}

export type IServiceAction = ISwitchAction | IMapChangeAction | IChangeCarTypeAction
    | IChangeDistrictAction | IChangePaymentMethodAction | IChangeCurrentPosition
    | IChangeSearchBoxAction | IChangeInfoDrawerIdAction | IMovePositionAction | ISaveMapAction