import { IServiceState, PaymentMethod } from "./state";
import { IServiceAction } from "./action";
import { CarType } from "./state";

const initialState: IServiceState = {
    drawerState: {
        menuOpen: false,
        infoOpen: false,
        carTypeOpen: false,
        paymentMethodOpen: false,
        specialRequirementOpen: false,
        districtOpen: false,
        hkOpen: false,
        klOpen: false,
        ntOpen: false,
        spOpen: false,
        carType: CarType.privateCar,
        paymentMethod: PaymentMethod.hourlyCharges,
        district: null,
        infoId: null,
        isEV: false,
        isDisable: false
    },
    mapState: {
        zoom: 12,
        center: {
            lat: 0,
            lng: 0
        },
        bounds: null,
        marginBounds: null
    },
    currentPosition: null,
    position: null,
    searchBoxValue: "",
    map: null,
    directionsDisplay: null,
}


export const serviceReducer = (state: IServiceState = initialState, action: IServiceAction): IServiceState => {
    switch (action.type) {
        case "SWITCH":
            if (action.CONTROL === "MENU") {
                return {
                    ...state,
                    drawerState: {
                        ...state.drawerState,
                        menuOpen: !state.drawerState.menuOpen
                    }
                }
            } else if (action.CONTROL === "INFO") {
                return {
                    ...state,
                    drawerState: {
                        ...state.drawerState,
                        infoOpen: !state.drawerState.infoOpen
                    }
                }
            } else if (action.CONTROL === "CARTYPE") {
                return {
                    ...state,
                    drawerState: {
                        ...state.drawerState,
                        carTypeOpen: !state.drawerState.carTypeOpen,
                        paymentMethodOpen: false,
                        districtOpen: false,
                        klOpen: false,
                        hkOpen: false,
                        ntOpen: false,
                        spOpen: false
                    }
                }
            } else if (action.CONTROL === "PAYMENT") {
                return {
                    ...state,
                    drawerState: {
                        ...state.drawerState,
                        paymentMethodOpen: !state.drawerState.paymentMethodOpen,
                        carTypeOpen: false,
                        districtOpen: false,
                        klOpen: false,
                        hkOpen: false,
                        ntOpen: false,
                        spOpen: false
                    }
                }
            } else if (action.CONTROL === "DISTRICT") {
                return {
                    ...state,
                    drawerState: {
                        ...state.drawerState,
                        districtOpen: !state.drawerState.districtOpen,
                        carTypeOpen: false,
                        paymentMethodOpen: false,
                        spOpen: false
                    }
                }
            } else if (action.CONTROL === "HK") {
                return {
                    ...state,
                    drawerState: {
                        ...state.drawerState,
                        districtOpen: true,
                        hkOpen: !state.drawerState.hkOpen,
                        klOpen: false,
                        ntOpen: false,
                    }
                }
            } else if (action.CONTROL === "KL") {
                return {
                    ...state,
                    drawerState: {
                        ...state.drawerState,
                        districtOpen: true,
                        klOpen: !state.drawerState.klOpen,
                        hkOpen: false,
                        ntOpen: false,
                    }
                }
            } else if (action.CONTROL === "NT") {
                return {
                    ...state,
                    drawerState: {
                        ...state.drawerState,
                        districtOpen: true,
                        ntOpen: !state.drawerState.ntOpen,
                        hkOpen: false,
                        klOpen: false,
                    }
                }
            } else if (action.CONTROL === "SP") {
                return {
                    ...state,
                    drawerState: {
                        ...state.drawerState,
                        spOpen: !state.drawerState.spOpen,
                        districtOpen: false,
                        carTypeOpen: false,
                        paymentMethodOpen: false,
                    }
                }
            } else if (action.CONTROL === "EV") {
                return {
                    ...state,
                    drawerState: {
                        ...state.drawerState,
                        isEV: !state.drawerState.isEV,
                        isDisable: false
                    }
                }
            } else if (action.CONTROL === "DISABLE") {
                return {
                    ...state,
                    drawerState: {
                        ...state.drawerState,
                        isDisable: !state.drawerState.isDisable,
                        isEV: false
                    }
                }
            } else {
            return state
            }
        case "MAPCHANGE":
            return {
                ...state,
                position: null,
                mapState: {
                    center: action.value.center,
                    zoom: action.value.zoom,
                    bounds: action.value.bounds,
                    marginBounds: action.value.marginBounds,
                }
            }
        case "CHANGE_CAR_TYPE":
            return {
                ...state,
                drawerState: {
                    ...state.drawerState,
                    districtOpen: false,
                    carType: action.value,
                }
            }
        case "CHANGE_PAYMENT_METHOD":
            return {
                ...state,
                drawerState: {
                    ...state.drawerState,
                    districtOpen: false,
                    paymentMethod: action.value,
                }
            }
        case "CHANGE_DISTRICT":
            return {
                ...state,
                drawerState: {
                    ...state.drawerState,
                    districtOpen: false,
                    district: action.value,
                },
                currentPosition: {
                    lat: action.coords.lat,
                    lng: action.coords.lng
                },
                mapState: {
                    ...state.mapState,
                    zoom: action.zoom
                }
            }

        case "CHANGE_CURRENT_POSITION":
            return {
                ...state,
                currentPosition: {
                    lat: action.value.latitude,
                    lng: action.value.longitude
                }
            }
        case "CHANGE_SEARCH_BOX":
            return {
                ...state,
                searchBoxValue: action.value
            }
        case "CHANGE_INFO_DRAWER_ID":
            return {
                ...state,
                drawerState: {
                    ...state.drawerState,
                    infoId: action.id
                }

            }
        case "MOVE_POSITION":
            return{
                ...state,
                position: {
                    lat: action.value.lat,
                    lng: action.value.lng
                }
            }
        case "SAVE_MAP":
            return {
                ...state,
                map: action.map,
                directionsDisplay: action.directionsDisplay
            }
        default:
            return state;
    }
}