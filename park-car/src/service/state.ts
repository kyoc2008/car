export interface IServiceState {
    drawerState: IDrawerState;
    mapState: IMapState;
    currentPosition: Coords | null;
    position: Coords | null;
    searchBoxValue: string;
    map: any;
    directionsDisplay: any;
}

export interface IDrawerState {
    menuOpen: boolean;
    infoOpen: boolean;
    carTypeOpen: boolean;
    paymentMethodOpen: boolean;
    specialRequirementOpen: boolean;
    districtOpen: boolean;
    hkOpen: boolean,
    klOpen: boolean,
    ntOpen: boolean,
    spOpen: boolean,
    carType: CarType,
    paymentMethod: PaymentMethod,
    district: District | null,
    infoId: string | null,
    isEV: boolean,
    isDisable: boolean
}

export enum CarType {
    privateCar = 'privateCar',
    lightGoodsVehicle = 'LGV',
    heavyGoodsVehicle = 'HGV',
    motorCycle = 'motorCycle',
}

export enum PaymentMethod {
    hourlyCharges = 'hourlyCharges',
    dayNightParks = 'dayNightParks',
    monthlyCharges = 'monthlyCharges',
}

export enum District {
    shamShuiPo  = 'shamShuiPo ',
    kowloonCity = 'kowloonCity',
    kwunTong = 'kwunTong',
    wongTaiSin = 'wongTaiSin',
    yauTsimMong = 'yauTsimMong',
    kwaiTsing = 'kwaiTsing',
    tuenWan = 'tuenWan',
    tuenMun = 'tuenMun',
    yuenLong = 'yuenLong',
    north = 'north',
    taiPo = 'taiPo',
    shaTin = 'shaTin',
    saiKung = 'saiKung',
    islands = 'islands',
    centralWestern = 'centralWestern',
    wanChai = 'wanChai',
    eastern = 'eastern',
    southern = 'southern'
}


export interface IMapState {
    center: Coords ;
    zoom: number;
    bounds: Bounds | null;
    marginBounds: Bounds | null;
}

export interface Coords {
    lat: number;
    lng: number;
}
export interface Bounds {
    nw: Coords;
    ne: Coords;
    sw: Coords;
    se: Coords;
}

