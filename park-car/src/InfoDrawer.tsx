import React from 'react';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import { withStyles, WithStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { IRootState, ThunkDispatch } from './store';
import { connect } from 'react-redux';
import { switchDrawer } from './service/action';
import InfoCard from './InfoCard';


const styles = {
    
};

interface Props extends WithStyles<typeof styles> {
    infoSwitch: () => void,
    infoOpen: boolean
}

class InfoDrawer extends React.Component<Props> {
   
    render() {
        return (
                <SwipeableDrawer
                    anchor="bottom"
                    open={this.props.infoOpen}
                    onClose={this.props.infoSwitch}
                    onOpen={this.props.infoSwitch}
                >
                    <InfoCard />
                </SwipeableDrawer>
        )
    }
}

const mapStateToProps = (state: IRootState) => {
    return {
        infoOpen: state.service.drawerState.infoOpen
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch) => ({
    infoSwitch: () => dispatch(switchDrawer("INFO"))
})


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(InfoDrawer));

