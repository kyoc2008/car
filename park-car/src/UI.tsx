import React from 'react';
import AppBar from "./AppBar";
import MenuDrawer from "./MenuDrawer";
import InfoDrawer from './InfoDrawer';
import { CssBaseline } from '@material-ui/core';


export default function UI(props: {}) {

    return(
        <div>
            <CssBaseline/>
            <AppBar />
            <MenuDrawer/> 
            <InfoDrawer/>
        </div>
    )
}